import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_jenkins_proxy_containers(host):
    with host.sudo():
        cmd = host.command('docker ps')
    assert cmd.rc == 0
    # Get the names of the running containers
    # - skip the first line (header)
    # - take the last element of the remaining lines
    names = sorted([line.split()[-1] for line in cmd.stdout.split('\n')[1:]])
    assert names == ['jenkins_proxy', 'traefik_proxy']


def test_jenkins_proxy_index(host):
    # This tests that traefik forwards traffic to the jenkins-proxy web server
    # and that we can access the jenkins-proxy status page
    cmd = host.command('curl -H Host:ics-ans-role-jenkins-proxy-default -k -L https://localhost')
    assert cmd.stdout == 'OK'
