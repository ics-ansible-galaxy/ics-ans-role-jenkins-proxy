ics-ans-role-jenkins-proxy
==========================

Ansible role to install jenkins-proxy.

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

Role Variables
--------------

```yaml
jenkins_proxy_user: ics
jenkins_proxy_group: ics
jenkins_proxy_processes: 3
jenkins_proxy_uwsgi_buffer_size: 16384
jenkins_proxy_settings: "{{ playbook_dir }}/config/settings.cfg"
jenkins_proxy_image: registry.esss.lu.se/ics-infrastructure/jenkins-proxy
jenkins_proxy_tag: latest
jenkins_proxy_hostname: "{{ ansible_fqdn }}"
```

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-jenkins-proxy
```

License
-------

BSD 2-clause
